export interface ICartItem {
  productId: string;
  quantity: number;
  color: string;
  size: string;
}

export interface ICart {
  userId: string;
  products: ICartItem[];
}
