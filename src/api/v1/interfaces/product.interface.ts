export interface IProduct {
  title: string;
  desc: string;
  img: string;
  categories?: string[];
  size?: string[];
  color?: string[];
  price: string;
  inStock: boolean;
  active: boolean;
}
