import { IUser } from "..";

export type RegisterInputType = Omit<IUser, "isAdmin">;
export type LoginInputType = Omit<IUser, "isAdmin" | "email">;
export type LoginResponse = Omit<IUser, "password">;
// type UserInputType = RegisterInputType | LoginInputType;
