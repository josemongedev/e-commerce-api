import { IProduct } from "interfaces";
export type ProductModelType = IProduct & Document;
