export interface ValidData<T> {
  data: T;
  valid: boolean;
}
