import { Document } from "mongoose";

export interface IUser extends Document {
  username: string;
  fullname: string;
  address: string;
  phone: string;
  email: string;
  password: string;
  isAdmin: boolean;
  active: boolean;
  img: string;
}
