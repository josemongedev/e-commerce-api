import { ICartItem } from "./cart.interface";
type IOrderItem = ICartItem;

export interface IOrder {
  userId: string;
  products: IOrderItem[];
  amount: number;
  address: object;
  status: string;
}
