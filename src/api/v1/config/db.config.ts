import mongoose from "mongoose";

export const connectOptions = {
  keepAlive: true,
  socketTimeoutMS: 2000,
  connectTimeoutMS: 2000,
};

export const dbConnect = async (
  URI: string
  // connectOptions: mongoose.ConnectOptions
) => {
  try {
    // console.log("Attempting connection to MongoDB");
    mongoose.set("strictQuery", false);
    await mongoose.connect(URI);
    // console.log("DB connection success!");
  } catch (err) {
    console.error(err);
  }
};
