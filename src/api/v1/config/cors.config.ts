import { CorsOptions } from "cors";

export const corsConfig: Record<string, any> = {};

corsConfig.allowedOrigins = "";

corsConfig.setAllowedOrigins = () => {
  // Only when env variables have been set and after being loaded
  if (process.env.ALLOWED_ORIGINS)
    corsConfig.allowedOrigins = process.env.ALLOWED_ORIGINS.split(",");
};

corsConfig.options = {
  origin: (origin, callback) => {
    if (
      (origin && corsConfig.allowedOrigins.indexOf(origin) !== -1) ||
      // origin is undefined when using localhost during development
      (!origin && process.env.NODE_ENV === "development") ||
      (!origin && process.env.NODE_ENV === "test")
    ) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  optionsSuccessStatus: 200,
  credentials: true,
} as CorsOptions;
