import { getTimeFrameObject, getIncomeObject } from "../helpers";
import { AuthRequest } from "../middlewares";
import {
  createOrder,
  deleteOrder,
  queryLastOrderByUserId,
  queryOrders,
  queryOrdersByUserId,
  queryOrdersIncome,
  queryOrdersIncomeById,
  updateOrder,
} from "../services";
import { Request, Response } from "express";
import { TokenCookieRequest } from "../middlewares/authCookie";

export const createOrderController = async (req: Request, res: Response) => {
  try {
    const newOrder = await createOrder(req.body);
    res.status(200).json(newOrder);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const updateOrderController = async (req: Request, res: Response) => {
  try {
    const updatedOrder = await updateOrder(req.params.id, req.body);
    res.status(200).json(updatedOrder);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const deleteOrderController = async (req: Request, res: Response) => {
  try {
    await deleteOrder(req.params.id);
    res.status(200).json("Order deleted");
  } catch (error) {
    res.status(500).json(error);
  }
};

export const getOrdersByUserIdController = async (
  req: AuthRequest & TokenCookieRequest,
  res: Response
) => {
  try {
    const cart = await queryOrdersByUserId(
      req!.user!.isAdmin ? req.params.userId : req!.user!.id
    );
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json(error);
  }
};

// GET last Order by user Id
export const getLastOrderController = async (
  req: AuthRequest,
  res: Response
) => {
  try {
    const orders = await queryLastOrderByUserId(req!.user!.id);
    res.status(200).json(orders);
  } catch (error) {
    res.status(500).json(error);
  }
};

// GET All Orders
export const getOrdersController = async (_req: Request, res: Response) => {
  try {
    const orders = await queryOrders();
    res.status(200).json(orders);
  } catch (error) {
    res.status(500).json(error);
  }
};

// GET Monthly Income
export const getMonthlyIncomeController = async (
  req: Request,
  res: Response
) => {
  const timeInterval = req?.query?.interval as string | null;
  const productId = req?.query?.pid as string | null;
  const timeframe = getTimeFrameObject(timeInterval);
  try {
    const dbIncome = productId
      ? await queryOrdersIncomeById(productId, timeframe.TwoMonthsAgo)
      : await queryOrdersIncome(timeframe.TwoMonthsAgo);
    const income = getIncomeObject(dbIncome, timeframe);
    res.status(200).json(income);
  } catch (error) {
    res.status(500).json(error);
  }
};
