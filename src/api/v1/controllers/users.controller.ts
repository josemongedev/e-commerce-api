import { Request, Response } from "express";
import { encryptAES } from "../helpers";
import jwt from "jsonwebtoken";
import { cookieConfig, TokenCookieRequest } from "../middlewares";
import {
  createUser,
  deleteUser,
  queryRecentUsers,
  queryUserById,
  queryUsers,
  queryUsersStats,
  updateUser,
  verifyCredentials,
} from "../services";
import { queryUser } from "../services/users.table";

export const checkStatusUserController = (
  req: TokenCookieRequest,
  res: Response
) => {
  // Store it and report data
  try {
    return res.status(200).json({ loggedIn: !!req?.token });
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
};

export const inUseUserController = async (req: Request, res: Response) => {
  // Store it and report data
  try {
    const user = await queryUser(req?.body?.username as string);
    console.log("user :>> ", user);
    return res.status(200).json({ inuse: !!user });
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
};

export const registerUserController = async (req: Request, res: Response) => {
  // Store it and report data
  try {
    const newUser = await createUser(req.body);
    const accessToken = jwt.sign(
      {
        id: newUser._id,
        isAdmin: newUser.isAdmin,
      },
      process.env.SECRET_JWT,
      { expiresIn: "3d" }
    );
    // Server cookie, login after registering user
    res.cookie("token", accessToken, cookieConfig);
    return res.status(201).json(newUser);
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
};

export const loginUserController = async (req: Request, res: Response) => {
  try {
    const userDbData: any = await verifyCredentials(req.body);
    if (userDbData?.username) {
      const accessToken = jwt.sign(
        {
          id: userDbData._id,
          isAdmin: userDbData.isAdmin,
        },
        process.env.SECRET_JWT,
        { expiresIn: "3d" }
      );
      // Server cookie to store JWT token
      res.cookie("token", accessToken, cookieConfig);
      return res.status(200).json(userDbData);
    } else {
      return res.status(401).json("Wrong credentials or non-existant user");
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
};

export const logoutUserController = async (_req: Request, res: Response) => {
  try {
    res.clearCookie("token");
    return res.status(200).json("User session logged out");
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
};

export const updateUserController = async (req: Request, res: Response) => {
  if (req?.body?.password) {
    req.body.password = encryptAES(req.body.password);
  }
  try {
    const updatedUser = await updateUser(req.params.id, req.body);
    res.status(200).json(updatedUser);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const deleteUserController = async (req: Request, res: Response) => {
  try {
    await deleteUser(req.params.id);
    res.status(200).json("User deleted");
  } catch (error) {
    res.status(500).json(error);
  }
};

export const getUserController = async (req: Request, res: Response) => {
  try {
    const dbUserById = await queryUserById(req.params.id);
    res.status(200).json(dbUserById);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const getUsersController = async (req: Request, res: Response) => {
  const query = req?.query?.new;
  try {
    const users = await (query ? queryRecentUsers() : queryUsers());
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const getUserStatsController = async (_req: Request, res: Response) => {
  const date = new Date();
  const lastYear = new Date(date.setFullYear(date.getFullYear() - 1));
  try {
    const stats = await queryUsersStats(lastYear);
    res.status(200).json(stats);
  } catch (error) {
    res.status(500).json(error);
  }
};
