import { AuthRequest, CartCookieRequest } from "../middlewares";
import {
  createCart,
  deleteCart,
  queryCartById,
  queryCartByUserId,
  queryCarts,
  updateCart,
} from "../services";
import { Request, Response } from "express";

export const createCartController = async (req: Request, res: Response) => {
  try {
    const newCart = await createCart(req.body);
    res.status(200).json(newCart);
  } catch (error) {
    res.status(500).json(error);
  }
};

// PUT User
export const updateCartController = async (req: Request, res: Response) => {
  try {
    console.log(req.body);
    const updatedCart = await updateCart(req?.params?.cartId, req.body);
    res.status(200).json(updatedCart);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
};

export const deleteCartController = async (req: Request, res: Response) => {
  try {
    await deleteCart(req.params.cartId);
    res.status(200).json("Cart deleted");
  } catch (error) {
    res.status(500).json(error);
  }
};

// Meant to be used after user checkout, before deleting this cart
export const readCartByIdController = async (
  req: AuthRequest & CartCookieRequest,
  res: Response
) => {
  try {
    const cartId = req.cookies.cartId;
    console.log("cartId from cookie:", cartId);
    console.log("cartId from params:", req.params.cartId);

    if (cartId === req.params.cartId || req.user?.isAdmin) {
      const cart = await queryCartById(req.params.cartId);
      return res.status(200).json(cart);
    }
    return res.status(403).json("You have no permissions for this request.");
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const readCartByUserIdController = async (
  req: Request,
  res: Response
) => {
  try {
    const cart = await queryCartByUserId(req.params.userId);
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const readCartsController = async (_req: Request, res: Response) => {
  try {
    const carts = await queryCarts();
    res.status(200).json(carts);
  } catch (error) {
    res.status(500).json(error);
  }
};
