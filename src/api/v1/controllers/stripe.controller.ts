import {
  createPendingOrder,
  declineOrder,
  fulfillOrder,
  getLineItemsFromCart,
} from "../helpers/order.processing";
import { AuthRequest } from "../middlewares";
import { Request, Response } from "express";
import Stripe from "stripe";

// Stripe seems to "forget" the SK, when creating the object initially
const stripe = new Stripe(process.env.STRIPE_SK, {
  apiVersion: "2020-08-27",
  typescript: true,
});

export const stripeWebhookController = async (req: Request, res: Response) => {
  const payload = req.body;
  const sig = req.headers["stripe-signature"] as any;
  let event: Stripe.Event;
  try {
    event = stripe.webhooks.constructEvent(
      payload,
      sig,
      process.env.WEBHOOK_SECRET
    );
  } catch (err) {
    console.log(`Error while stripe webhook event: Message:${err.message}`);
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }

  try {
    const session = event?.data?.object as Stripe.Checkout.Session;
    // console.log("Session metadata: ");
    // console.dir(session.metadata);
    switch (event.type) {
      case "checkout.session.completed": {
        // Save an order in your database, marked as 'awaiting payment'
        await createPendingOrder(session);

        if (session.payment_status === "paid") {
          await fulfillOrder(session);
        }
        break;
      }

      case "checkout.session.async_payment_succeeded": {
        // Fulfill the purchase...
        await fulfillOrder(session);
        break;
      }

      case "checkout.session.async_payment_failed": {
        // Cancel order, if possible notify user
        await declineOrder(session);
        break;
      }
      case "checkout.session.expired":
        // Then define and call a function to handle the event checkout.session.expired
        await declineOrder(session);
        break;
      // ... handle other event types
      default:
        console.log(`Unhandled event type ${event.type}`);
    }
  } catch (error) {
    return res.status(500).send(error);
  }
  return res.status(200).send({ received: true });
};

export const readStripePkController = (
  _req: AuthRequest,
  res: Response
): void => {
  // Serve checkout page.
  res.send({
    publishableKey: process.env.STRIPE_PK,
  });
};

export const createCheckoutSessionController = async (
  req: AuthRequest,
  res: Response
) => {
  try {
    const { lineItems, cartId } = await getLineItemsFromCart(req.user?.id);
    if (cartId) {
      res.cookie("cartId", cartId, {
        domain: "api-sadhana-shop.herokuapp.com",
        path: "/",
        httpOnly: true,
        sameSite: "none",
        secure: true,
      });
      const session: Stripe.Response<Stripe.Checkout.Session> =
        await stripe.checkout.sessions.create(
          {
            line_items: lineItems,
            metadata: {
              userId: req!.user!.id,
              cartId: cartId,
            },
            mode: "payment",
            success_url: `${process.env.FRONTEND_URL}/success/${cartId}`,
            cancel_url: `${process.env.FRONTEND_URL}/cart`,
          },
          { apiKey: process.env.STRIPE_SK }
        );
      res.redirect(303, session.url as string);
    } else {
      res.status(400).send("No valid cart found");
    }
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
};
