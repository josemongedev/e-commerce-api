import {
  createProduct,
  deleteProduct,
  queryProductById,
  queryProducts,
  queryProductsByCategories,
  queryProductsByIdList,
  // queryProductsByIdList,
  queryRecentProducts,
  updateProduct,
} from "../services";
import { Request, Response } from "express";
import { ObjectId } from "mongodb";

export const createProductController = async (req: Request, res: Response) => {
  try {
    const newProduct = await createProduct(req.body);
    res.status(200).json(newProduct);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const updateProductController = async (req: Request, res: Response) => {
  try {
    const updatedProduct = await updateProduct(req.params.id, req.body);
    res.status(200).json(updatedProduct);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const deleteProductController = async (req: Request, res: Response) => {
  try {
    await deleteProduct(req.params.id);
    res.status(200).json("Product deleted");
  } catch (error) {
    res.status(500).json(error);
  }
};

export const readProductController = async (req: Request, res: Response) => {
  try {
    const product = await queryProductById(req.params.id);
    res.status(200).json(product);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const readProductsListController = async (
  req: Request,
  res: Response
) => {
  const qIdList = req?.body?.list.map((pid: string) => new ObjectId(pid));

  try {
    const products = await queryProductsByIdList(qIdList);
    res.status(200).json(products);
  } catch (error) {
    res.status(500).json(error);
  }
};

export const readProductsController = async (req: Request, res: Response) => {
  const qNew = req?.query?.new;
  const qCategory = req?.query?.category;
  try {
    const products = await (qNew
      ? queryRecentProducts()
      : qCategory
      ? queryProductsByCategories(qCategory)
      : queryProducts());

    res.status(200).json(products);
  } catch (error) {
    res.status(500).json(error);
  }
};
