import express, { Router } from "express";
import { authRouter } from "./auth.route";
import { cartsRouter } from "./carts.route";
import { checkoutRouter } from "./checkout.route";
import { ordersRouter } from "./orders.route";
import { productsRouter } from "./products.route";
import { usersRouter } from "./users.route";

export const apiRouter: Router = express.Router();

apiRouter.use("/auth", authRouter);
apiRouter.use("/users", usersRouter);
apiRouter.use("/products", productsRouter);
apiRouter.use("/carts", cartsRouter);
apiRouter.use("/orders", ordersRouter);
apiRouter.use("/checkout", checkoutRouter);
apiRouter.get("/ping", (_, res) => res.json({ message: "Server is up" }));
apiRouter.get("/", (_, res) =>
  res.json({ status: "Sadhana API v1 online. Connected to MongoDB" })
);
