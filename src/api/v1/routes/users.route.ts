import {
  deleteUserController,
  getUserController,
  getUsersController,
  getUserStatsController,
  updateUserController,
} from "../controllers";
import { verifyTokenAndAdmin, verifyTokenAndAuth } from "../middlewares";
import express, { Router } from "express";

export const usersRouter: Router = express.Router();

// PUT User
usersRouter.put("/:id", verifyTokenAndAuth, updateUserController);

// DELETE User
usersRouter.delete("/:id", verifyTokenAndAuth, deleteUserController);

// GET User
usersRouter.get("/find/:id", verifyTokenAndAuth, getUserController);

// GET All Users
usersRouter.get("/", verifyTokenAndAdmin, getUsersController);

// GET User Stats
usersRouter.get("/stats", verifyTokenAndAdmin, getUserStatsController);
