import {
  verifyToken,
  verifyTokenAndAdmin,
  verifyTokenAndAuth,
} from "../middlewares";
import express, { Router } from "express";
import {
  createOrderController,
  deleteOrderController,
  getLastOrderController,
  getMonthlyIncomeController,
  getOrdersByUserIdController,
  getOrdersController,
  updateOrderController,
} from "../controllers";

export const ordersRouter: Router = express.Router();

// GET User Orders
ordersRouter.get("/last", verifyTokenAndAuth, getLastOrderController);

// GET  Orders by USER ID (non admin)
ordersRouter.get(
  "/user/:userId",
  verifyTokenAndAuth,
  getOrdersByUserIdController
);

// POST Order
ordersRouter.post("/", verifyToken, createOrderController);

// PUT Order
ordersRouter.put("/:id", verifyTokenAndAuth, updateOrderController);

// DELETE Order
ordersRouter.delete("/:id", verifyTokenAndAuth, deleteOrderController);

// GET Order (admin rights)
ordersRouter.get(
  "/find/:userId",
  verifyTokenAndAuth,
  getOrdersByUserIdController
);

// GET Monthly Income
ordersRouter.get("/income", verifyTokenAndAdmin, getMonthlyIncomeController);

// GET All Orders
ordersRouter.get("/", verifyTokenAndAdmin, getOrdersController);
