import express, { Router } from "express";
import {
  createCheckoutSessionController,
  readStripePkController,
} from "../controllers";
import { verifyTokenAndAuth } from "../middlewares";

export const checkoutRouter: Router = express.Router();

checkoutRouter.get("/config", readStripePkController);

checkoutRouter.post(
  "/create-checkout-session",
  verifyTokenAndAuth,
  createCheckoutSessionController
);
