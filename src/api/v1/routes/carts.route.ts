import {
  createCartController,
  deleteCartController,
  readCartByIdController,
  readCartByUserIdController,
  readCartsController,
  updateCartController,
} from "../controllers/carts.controller";
import {
  verifyToken,
  verifyTokenAndAdmin,
  verifyTokenAndAuth,
} from "../middlewares";
import express, { Router } from "express";

export const cartsRouter: Router = express.Router();

// POST Cart
cartsRouter.post("/", verifyToken, createCartController);

// PUT Cart
cartsRouter.put("/:cartId", verifyTokenAndAuth, updateCartController);

// DELETE Cart
cartsRouter.delete("/:cartId", verifyTokenAndAuth, deleteCartController);

// GET Cart by ID
cartsRouter.get("/:cartId", verifyTokenAndAuth, readCartByIdController);

// GET Cart by User ID
cartsRouter.get(
  "/find/:userId",
  verifyTokenAndAuth,
  readCartByUserIdController
);

// GET All Carts
cartsRouter.get("/", verifyTokenAndAdmin, readCartsController);
