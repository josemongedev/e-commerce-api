import { stripeWebhookController } from "../../controllers";
import express, { Router } from "express";

export const checkoutHookRouter: Router = express.Router();

checkoutHookRouter
  .use(express.raw({ type: "application/json" }))
  .post("/", stripeWebhookController);
