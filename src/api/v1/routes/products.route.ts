import {
  createProductController,
  deleteProductController,
  readProductController,
  readProductsController,
  readProductsListController,
  updateProductController,
} from "../controllers";
import { verifyTokenAndAdmin } from "../middlewares";
import express, { Router } from "express";

export const productsRouter: Router = express.Router();

productsRouter.post("/", verifyTokenAndAdmin, createProductController);

// PUT Product
productsRouter.put("/:id", verifyTokenAndAdmin, updateProductController);

// DELETE Product
productsRouter.delete("/:id", verifyTokenAndAdmin, deleteProductController);

// GET Product by Id
productsRouter.get("/find/:id", readProductController);

// GET Product by IDs list
productsRouter.post("/list", readProductsListController);

// GET All Products
productsRouter.get("/", readProductsController);
