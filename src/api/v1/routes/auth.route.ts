import express, { Router } from "express";
import {
  loginUserController,
  logoutUserController,
  registerUserController,
  checkStatusUserController,
  inUseUserController,
} from "../controllers";
import { getAuthTokenFromCookie } from "../middlewares/authCookie";

export const authRouter: Router = express.Router();

// Check Status
authRouter.get("/check", getAuthTokenFromCookie, checkStatusUserController);

// User account exists
authRouter.post("/username", inUseUserController);

// Register
authRouter.post("/register", registerUserController);

// Login
authRouter.post("/login", loginUserController);

// Logout
authRouter.post("/logout", logoutUserController);
