import { RECENT_USERS_LIMIT } from "../constant";
import { decryptAES, encryptAES } from "../helpers/encrypt";
import { IUser, LoginInputType, RegisterInputType } from "../interfaces";
import { UserModel } from "../models";

export const createUser = (user: RegisterInputType) => {
  // Create new user object
  user.password = encryptAES(user.password);
  return new UserModel(user).save();
};

export const queryUserById = async (id: any) => {
  // Fetch existing user object
  const usr: any = await UserModel.findById(id);
  if (!usr) throw "Error fetching user";
  const { password, ...userData } = usr?._doc;
  return userData;
};

export const queryUser = async (username: string) =>
  // Fetch existing user object
  await UserModel.findOne({ username });

export const queryUsers = async () =>
  // Fetch existing users objects
  await UserModel.find();

export const queryRecentUsers = async () =>
  // Fetch existing recent users objects
  await UserModel.find().sort({ _id: -1 }).limit(RECENT_USERS_LIMIT);

export const queryUsersStats = async (yearDate: Date) =>
  // Fetch existing users objects
  await UserModel.aggregate([
    { $match: { createdAt: { $gte: yearDate } } },
    { $project: { month: { $month: "$createdAt" } } },
    { $group: { _id: "$month", total: { $sum: 1 } } },
    { $sort: { _id: 1 } },
  ]);

export const updateUser = async (id: any, body: Partial<IUser>) => {
  // Update user with new values
  return await UserModel.findByIdAndUpdate(id, { $set: body }, { new: true });
};

export const deleteUser = async (id: any) => {
  // Delete user from DB
  return await UserModel.findByIdAndDelete(id);
};

export const verifyCredentials = async (
  validUser: LoginInputType
): Promise<any | null> => {
  const user: any = await queryUser(validUser.username);
  if (!user) return null;
  const { password, ...userData } = user?._doc;
  if (!password) return null;
  return decryptAES(password) === validUser.password ? userData : {};
};
