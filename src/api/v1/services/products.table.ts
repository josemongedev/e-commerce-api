import { RECENT_PRODUCTS_LIMIT } from "../constant";
import { IProduct } from "../interfaces";
import { ProductModel } from "../models";

export const createProduct = (product: IProduct) => {
  return new ProductModel(product).save();
};

export const queryProductById = async (id: any) =>
  await ProductModel.findById(id);

export const queryProductByTitle = async (title: string) =>
  // Fetch existing product object
  await ProductModel.findOne({ title });

export const queryProducts = async () =>
  // Fetch existing products objects
  await ProductModel.find();

export const queryRecentProducts = async () =>
  // Fetch existing recent products objects
  await ProductModel.find().sort({ _id: -1 }).limit(RECENT_PRODUCTS_LIMIT);

export const queryProductsByCategories = async (category: any) =>
  // Fetch existing products by category
  await ProductModel.find({ categories: { $in: [category] } });

export const queryProductsByIdList = async (idList: string[]) =>
  // Fetch existing products by Id
  await ProductModel.find({ _id: { $in: idList } });

export const updateProduct = async (id: any, body: Partial<IProduct>) => {
  // Update product with new values
  return await ProductModel.findByIdAndUpdate(
    id,
    { $set: body },
    { new: true }
  );
};

export const deleteProduct = async (id: any) => {
  // Delete product from DB
  return await ProductModel.findByIdAndDelete(id);
};
