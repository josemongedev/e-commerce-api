import { IOrder } from "../interfaces";
import { OrderModel } from "../models";

export const createOrder = (product: IOrder) => {
  return new OrderModel(product).save();
};

export const queryLastOrderByUserId = async (userId: any) =>
  await OrderModel.find({ userId }).sort({ _id: -1 }).limit(1);

export const queryOrdersByUserId = async (userId: any) =>
  await OrderModel.find({ userId });

export const queryOrders = async () => await OrderModel.find();

export const updateOrder = async (id: any, body: Partial<IOrder>) => {
  // Update product with new values
  return await OrderModel.findByIdAndUpdate(id, { $set: body }, { new: true });
};

export const deleteOrder = async (id: any) => {
  // Delete product from DB
  return await OrderModel.findByIdAndDelete(id);
};

export const queryOrdersIncome = async (timeFrameStart: Date) =>
  await OrderModel.aggregate([
    {
      $match: { createdAt: { $gte: timeFrameStart } },
    },
    { $project: { month: { $month: "$createdAt" }, sales: "$amount" } },
    { $group: { _id: "$month", total: { $sum: "$sales" } } },
  ]);

export const queryOrdersIncomeById = async (id: string, timeFrameStart: Date) =>
  await OrderModel.aggregate([
    {
      $match: {
        createdAt: { $gte: timeFrameStart },
        products: { $elemMatch: { productId: id } },
      },
    },
    { $project: { month: { $month: "$createdAt" }, sales: "$amount" } },
    { $group: { _id: "$month", total: { $sum: "$sales" } } },
  ]);
