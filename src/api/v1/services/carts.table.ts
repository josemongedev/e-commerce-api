import { ICart } from "../interfaces";
import { CartModel } from "../models";

export const createCart = (product: ICart) => {
  return new CartModel(product).save();
};

export const queryCartByUserId = async (userId: string) =>
  await CartModel.findOne({ userId });

export const queryCartById = async (cartId: string) =>
  await CartModel.findById(cartId);

export const queryCarts = async () =>
  // Fetch existing products objects
  await CartModel.find();

export const updateCart = async (id: string, body: Partial<ICart>) => {
  // Update product with new values
  return await CartModel.findByIdAndUpdate(id, { $set: body }, { new: true });
};

export const deleteCart = async (id: any) => {
  // Delete product from DB
  return await CartModel.findByIdAndDelete(id);
};
