import { CookieOptions, NextFunction, Request, Response } from "express";
import { IUser } from "../interfaces/user.interface";

export const cookieConfig: CookieOptions = {
  // domain: "api-sadhana.josemonge.dev",
  // domain: "localhost:3000",
  // path: "/",
  httpOnly: true,
  sameSite: "none",
  // sameSite: "strict",
  expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 3), // 3 days
  secure: true,
};

export interface TokenCookieRequest extends Request {
  token?: string;
}
export interface CartCookieRequest extends Request {
  cartId?: string;
}
export interface AuthRequest
  extends Request,
    TokenCookieRequest,
    CartCookieRequest {
  token?: string;
  user?: IUser;
}

export const getAuthTokenFromCookie = (
  req: TokenCookieRequest,
  res: Response,
  next: NextFunction
) => {
  try {
    req.token = req.cookies.token;
    // req.cartId = req.cookies.cartId;
    return next();
  } catch (error) {
    res.clearCookie("token");
    res.clearCookie("cartId");
    return res.status(401).json("No authentication has been done");
  }
};
