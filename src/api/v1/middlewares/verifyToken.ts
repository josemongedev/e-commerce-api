import { NextFunction, Response } from "express";
import jwt from "jsonwebtoken";
import { AuthRequest, getAuthTokenFromCookie } from "./authCookie";
import { IUser } from "../interfaces/user.interface";

export const verifyToken = (
  req: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  getAuthTokenFromCookie(req, res, () => {
    const authToken = req.token as string;
    if (!authToken) return res.status(401).json("You are not authenticated");
    return jwt.verify(authToken, process.env.SECRET_JWT, (error, user) => {
      if (error) return res.status(401).json("Token is invalid or expired!");
      req.user = user as IUser;
      return next();
    });
  });
};

export const verifyTokenAndAuth = (
  req: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  verifyToken(req, res, () => {
    if (req?.user?.id === req.params.id || req?.user?.isAdmin) return next();
    return res.status(403).json({
      loggedIn: false,
      error: "You have no permissions for that operation",
    });
  });
};

export const verifyTokenAndAdmin = (
  req: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  verifyToken(req, res, () => {
    if (req?.user?.isAdmin) return next();
    return res.status(403).json({
      loggedIn: false,
      error: "You have no permissions for that operation",
    });
  });
};
