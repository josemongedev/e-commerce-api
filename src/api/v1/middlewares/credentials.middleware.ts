import { StatusCodes } from "http-status-codes";
import { RequestHandler } from "express";
import { corsConfig } from "../config/cors.config";

const accessControlHeaders = (
  origin: string,
  enabled: boolean,
  age: number
): Record<string, string> => ({
  "Access-Control-Allow-Origin": origin,
  "Access-Control-Allow-Credentials": String(enabled),
  "Access-Control-Allow-Headers":
    "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization",
  // "Origin, X-Requested-With, Content-Type, Accept, Authorization",
  // "Origin, X-Requested-With, Content-Type, Accept",
  //   "X-CSRF-Token, X-CSRF-Strategy, X-Requested-With, Accept, Authorization, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version"
  "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,UPDATE,OPTIONS",
  "Access-Control-Allow-Max-Age": String(age),
});

export const credentials: RequestHandler = (req, res, next) => {
  const origin = req?.headers?.origin as string;
  if (corsConfig.allowedOrigins.includes(origin)) {
    Object.entries(accessControlHeaders(origin, true, 3600)).forEach(
      ([headerKey, headerValue]) => {
        res.header(headerKey, headerValue);
      }
    );
  }
  res.set("Content-Type", "application/json");

  return req && req.method === "OPTIONS"
    ? res.sendStatus(StatusCodes.NO_CONTENT)
    : next();
};
