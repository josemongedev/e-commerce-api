import { Schema, model } from "mongoose";
import { IUser } from "../interfaces";

// Schema corresponding to the document interface.
const UserSchema = new Schema<IUser>(
  {
    username: { type: String, required: true, unique: true },
    fullname: { type: String, default: "" },
    address: { type: String, default: "" },
    phone: { type: String, default: "" },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    isAdmin: {
      type: Boolean,
      default: false,
    },
    active: {
      type: Boolean,
      default: true,
    },
    img: { type: String },
  },
  { timestamps: true }
);

// Model.
export const UserModel = model<IUser>("User", UserSchema);
