import { Schema, model } from "mongoose";
import { IOrder } from "../interfaces";

// Schema corresponding to the document interface.
const OrderSchema = new Schema<IOrder>(
  {
    userId: { type: String, required: true },
    products: [
      {
        productId: {
          type: String,
        },
        quantity: {
          type: Number,
          default: 1,
        },
      },
    ],
    amount: { type: Number, required: true },
    address: { type: String, required: true },
    status: { type: String, default: "pending" },
  },
  { timestamps: true }
);

// Model.
export const OrderModel = model<IOrder>("Order", OrderSchema);
