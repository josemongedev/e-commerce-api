import { Schema, model } from "mongoose";
import { ICart } from "../interfaces";

// Schema corresponding to the document interface.
const CartSchema = new Schema<ICart>(
  {
    userId: { type: String, required: true },
    products: [
      {
        productId: {
          type: String,
        },
        quantity: {
          type: Number,
          default: 1,
        },
        color: {
          type: String,
        },
        size: {
          type: String,
        },
      },
    ],
  },
  { timestamps: true }
);

// Model.
export const CartModel = model<ICart>("Cart", CartSchema);
