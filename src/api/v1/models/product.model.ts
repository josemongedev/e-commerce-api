import { Schema, model } from "mongoose";
import { ProductModelType } from "../interfaces";

// Schema corresponding to the document interface.
const ProductSchema = new Schema<ProductModelType>(
  {
    title: { type: String, required: true, unique: true },
    desc: { type: String, required: true },
    img: { type: String, required: true },
    categories: [String],
    size: { type: Array },
    color: { type: Array },
    price: { type: String, required: true },
    inStock: { type: Boolean, default: true },
    active: { type: Boolean, default: true },
  },
  { timestamps: true }
);

// Model.
export const ProductModel = model<ProductModelType>("Product", ProductSchema);
