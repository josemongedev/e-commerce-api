import CryptoJS from "crypto-js";

export const encryptAES = (data: string) =>
  CryptoJS.AES.encrypt(data, process.env.SECRET_KEY_PASS).toString();

export const decryptAES = (data: string): string =>
  CryptoJS.AES.decrypt(data, process.env.SECRET_KEY_PASS).toString(
    CryptoJS.enc.Utf8
  );
