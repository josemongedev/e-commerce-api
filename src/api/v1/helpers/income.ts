export interface ITimeframe {
  TwoMonthsAgo: Date;
  OneMonthAgo: Date;
  CurrentDate: Date;
}
export interface IIncomeRegister {
  _id: number;
  total: number;
}

export const getTimeFrameObject = (interval: string | null): ITimeframe => {
  if (interval === "yearly") return {} as ITimeframe;
  const baseDate = new Date();
  const OneMonthAgo = new Date(baseDate.setMonth(baseDate.getMonth() - 1));
  return {
    CurrentDate: new Date(),
    OneMonthAgo,
    TwoMonthsAgo: new Date(new Date().setMonth(baseDate.getMonth() - 1)),
  };
};

export const getIncomeObject = (
  dbIncome: IIncomeRegister[],
  timeframe: ITimeframe
) => {
  const monthlyIncome: IIncomeRegister[] = [
    { _id: timeframe.OneMonthAgo.getMonth(), total: 0 },
    { _id: timeframe.CurrentDate.getMonth(), total: 0 },
  ];

  monthlyIncome.map((month) =>
    dbIncome.find((item) => {
      if (item._id - 1 === month._id) month.total = item.total;
    })
  );
  return monthlyIncome;
};
