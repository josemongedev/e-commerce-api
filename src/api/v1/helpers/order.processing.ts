import { ICart, IOrder } from "../interfaces";
import {
  createOrder,
  queryCartById,
  queryCartByUserId,
  queryLastOrderByUserId,
  queryProductsByIdList,
  queryUserById,
  updateOrder,
} from "../services";
import Stripe from "stripe";

export const fulfillOrder = async (session: Stripe.Checkout.Session) => {
  const userId = session!.metadata!.userId;
  const lastOrder = await queryLastOrderByUserId(userId);
  await updateOrder(lastOrder[0]._id, { status: "approved" });
};

export const createPendingOrder = async (session: Stripe.Checkout.Session) => {
  const userId = session!.metadata!.userId;
  const user = await queryUserById(userId);
  const address = user.address || session!.shipping_address_collection || "";
  const cartId = session!.metadata!.cartId;
  const amount = (session!.amount_total as number) / 100;
  const status = "pending";
  const { products } = (await queryCartById(cartId)) as ICart;
  const newOrder: IOrder = { userId, products, amount, address, status };
  await createOrder(newOrder);
};

export const declineOrder = async (session: Stripe.Checkout.Session) => {
  const userId = session!.metadata!.userId;
  const lastOrder = await queryLastOrderByUserId(userId);
  await updateOrder(lastOrder[0]._id, { status: "declined" });
};

export const getLineItemsFromCart = async (userId: string) => {
  const cart = await queryCartByUserId(userId);
  const idList = cart!.products!.map((item) => item.productId);
  const productData = await queryProductsByIdList(idList);
  const lineItems: Stripe.Checkout.SessionCreateParams.LineItem[] =
    cart!.products.map((item) => {
      const pData = productData.find(
        (product) => product._id.toString() === item.productId
      );
      return {
        price_data: {
          currency: "usd",
          product_data: {
            name: pData?.title as string,
          },
          unit_amount: parseInt(pData?.price as string) * 100,
        },
        quantity: item.quantity,
      };
    });
  // const moreProductData = cart!.products.map(({ productId, size, color }) => ({
  //   productId,
  //   size,
  //   color,
  // }));
  return { lineItems, cartId: cart!._id.toString() };
};
