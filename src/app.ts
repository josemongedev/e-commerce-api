import dotenv from "dotenv";
dotenv.config();

import cookieParser from "cookie-parser";
import cors from "cors";
import express, { Application } from "express";
import morgan from "morgan";
import { corsConfig } from "./api/v1/config";
import { credentials } from "./api/v1/middlewares/credentials.middleware";
import { apiRouter as apiRouter } from "./api/v1/routes";
import { checkoutHookRouter } from "./api/v1/routes/hooks/checkout.route";
import { errorHandler, notFound } from "./api/v1/middlewares/common.middleware";
import { StatusCodes } from "http-status-codes";
import { dbStart } from "./api/v1/middlewares/db.middleware";

const app: Application = express();

//Middlewares
app.use(cookieParser());
app.use(morgan("combined"));
corsConfig.setAllowedOrigins();
app.use(cors(corsConfig.options));
app.use(credentials);

// Root endpoint
app.get("/", (_, res) => res.sendStatus(StatusCodes.OK));

// Connects to database as a middleware (for every request: required for vercel's serverless function)
//https://www.mongodb.com/developer/languages/javascript/integrate-mongodb-vercel-functions-serverless-experience/
app.use(dbStart);

// Webhook Routes (stripe hook)
app.use("/hook/checkout", checkoutHookRouter);

// API Routes
// Only parse body for API, not stripe webhooks
app.use(express.json()).use("/api/v1", apiRouter);

// Other routes
app.use(notFound);
app.use(errorHandler);

export default app;
