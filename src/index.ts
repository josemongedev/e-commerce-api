import app from "app";

// Start server listening...
const port = process.env.PORT || 5000;
app.listen(port, () => {
  /* eslint-disable no-console */
  console.log(`Backend server running on http://localhost:${port}`);
  /* eslint-enable no-console */
});
