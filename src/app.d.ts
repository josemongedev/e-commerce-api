export {};
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      [k: string]: string;
      readonly DATABASE_URI: string;
      readonly ALLOWED_ORIGINS: string;
      readonly TS_NODE_BASEURL:string;
      readonly PORT: string;
      readonly FRONTEND_URL:string;
      readonly ALLOWED_ORIGINS:string;
      readonly SECRET_KEY_PASS:string;
      readonly SECRET_JWT:string;
      readonly STRIPE_PK:string;
      readonly STRIPE_SK:string;
      readonly WEBHOOK_SECRET:string;
    }
  }
}
